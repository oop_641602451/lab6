package com.tiwter.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank tiwter = new BookBank("Tiwter", 50.0);

        tiwter.print();
        BookBank prayud = new BookBank("Prayud",100000.0);
        prayud.print();
        prayud.withdraw(40000.0);
        prayud.print();

        tiwter.deposit(40000.0);
        tiwter.print();

        BookBank prawit = new BookBank("Prawit",1000000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();
    }
}
