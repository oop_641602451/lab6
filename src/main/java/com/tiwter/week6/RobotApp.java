package com.tiwter.week6;

import javax.swing.event.SwingPropertyChangeSupport;

public class RobotApp {
    public static void main(String[] args) {
        Robot kapong = new Robot("Kapong", 'K', 10, 10);
        Robot blue = new Robot("Blue", 'B' ,1 ,1);
        kapong.print();
        kapong.right();   
        kapong.print();
        blue.print();


        for(int y = Robot.MIN_Y; y<Robot.MAX_Y; y++) {
            for(int x = Robot.MIN_Y; x<Robot.MAX_Y; x++) {
                if(kapong.getX() == x && kapong.getY() == y) {
                System.out.print(kapong.getSymbol()); 
                } else if(blue.getX() == x && blue.getY() == y){
                    System.out.print(blue.getSymbol());
                } else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
}
