package com.tiwter.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldDownOver() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }

    @Test
    public void shouldUpNegative() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getX());
    }

    @Test
    public void shouldLeft() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MIN_X);
        assertEquals(false, robot.left());
        assertEquals(Robot.MIN_X, robot.getX());
    }

    @Test
    public void shouldDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }

    @Test
    public void shouldUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRightSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.right());
        assertEquals(1, robot.getX());
    }

    @Test
    public void shouldRobotUpFailMin() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());
    }

    @Test
    public void shouldRobotUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(true, robot.up());
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldRobotUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(true, robot.up(5));
        assertEquals(6, robot.getY());
    }

    @Test
    public void shouldRobotUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(true, robot.up(11));
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(false, robot.up(12));
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        assertEquals(true, robot.down());
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldRobotDownNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        assertEquals(true, robot.down(1));
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldRobotLeftSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(true, robot.left());
        assertEquals(9, robot.getX());
    }

    @Test
    public void shouldRobotLeftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(true, robot.left(1));
        assertEquals(9, robot.getX());
    }

    @Test
    public void shouldRobotLeftNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(false, robot.left(12));
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRobotRightSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        assertEquals(true, robot.right());
        assertEquals(11, robot.getX());
    }

    @Test
    public void shouldRobotRightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        assertEquals(true, robot.right(1));
        assertEquals(11, robot.getX());
    }

    @Test
    public void shouldRobotRightNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        assertEquals(false, robot.right(10));
        assertEquals(19, robot.getX());
    }
}
