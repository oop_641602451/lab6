package com.tiwter.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldwithdrawSuccess() {
        BookBank book = new BookBank("Tiwter", 100);
        book.withdraw(50);
        assertEquals(50, book.getBalance(), 0.0001);
    }

    @Test
    public void shouldwithdrawOver() {
        BookBank book = new BookBank("Tiwter", 100);
        book.withdraw(150);
        assertEquals(100, book.getBalance(), 0.00001);
    }
    
    @Test
    public void shouldwithdrawNagativeNumber() {
        BookBank book = new BookBank("Tiwter", 100);
        book.withdraw(-100);
        assertEquals(100, book.getBalance(), 0.00001);
    }
}
